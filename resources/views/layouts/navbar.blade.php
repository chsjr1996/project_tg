<nav class="navbar navbar-expand-md navbar-light navbar-laravel col-10 custom-css fix-position fixed-top">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Center Of Navbar -->
            <ul class="navbar-nav w-100">
                <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
            </ul>
        </div>
    </div>
</nav>